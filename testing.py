import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome("C:\\Users\\User\\FILES\\2ndyear\\SOFTWAREQUALITYENG\\chromedriver.exe") 

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://danibudi.github.io/Cross-Site%20Scripting%20(safe%20XSS).html")
        self.assertIn("django", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("pycon")
        elem.send_keys(Keys.RETURN)
        assert "No results found." not in driver.page_source


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()