# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class QAExam(unittest.TestCase):
	#To run test you : open cmd and
	#C:\Users\User>python C:\Users\User\FILES\2ndyear\SOFTWAREQUALITYENG\QA_mock1.py".
	#and it starts to run. Below is how to commit the test code in bitbucket.
	#C:\Users\User\FILES\2ndyear\SOFTWAREQUALITYENG>git commit -m "ammended test".
	#git remote add origin (copy link address from bit bucket and paste).
	#git pull
	#git pull origin master
	#git push --set-upstream origin master.
	#git gui
	#stage a commit
	#fetch from origin and push.
	#For jenkins you paste (java-jar jenkins.war)



    def setUp(self):
        self.driver = webdriver.Chrome("C:\\Users\\User\\FILES\\2ndyear\\SOFTWAREQUALITYENG\\chromedriver.exe") 
        self.driver.implicitly_wait(50) # use for loading the page with the given time of 30 sec.
        self.base_url = "https://danibudi.github.io/Cross-Site%20Scripting%20(safe%20XSS).html"
        #~ self.verificationErrors = []
        #~ self.accept_next_alert = True
    
    def test_ss(self):
        driver = self.driver
        driver.get(self.base_url)
        time.sleep (10)
        driver.save_screenshot("First_Screenshot.png")
        driver.get("https://danibudi.github.io/Cross-Site%20Scripting%20(safe%20XSS).html")
        alert = driver.switch_to_alert("OK")
        
        click(on_element="p")
        

        driver.get("https://danibudi.github.io/Cross-Site%20Scripting%20(safe%20XSS).html")
        self.assertEqual("django", driver.getTitle(""));

        #driver.find_element_by_id("generic").click()
        #time.sleep(10)

        #self.assertEqual("https://docs.djangoproject.com/en/2.0/topics/security/")
        #driver.find_element_by_id("generic").send_keys('selenium')
         #here you can enter url
        #search = driver.find_element_by_id("")
        #search.send_keys('selenium')
        #search.send_keys(Keys.ENTER)
        """#self.assertEqual(element.text,'')
		
        driver.get(self.base_url + "/#/127")
        lc = driver.find_element_by_xpath('//*[@id="rso"]/div[1]/div/div[1]/div/div/div/div/span')
        ls.click()
        driver.find_element_by_link_text("Cross-Site Scripting (safe XSS).html").send_keys(Keys.ENTER)

        alertAccepted = self.close_alert_and_get_its_text()
        self.assertEqual(alertAccepted,'42')
        driver.save_screenshot("Fourth_Screenshot.png")
        element = driver.find_element_by_tag_name("h2")
        self.assertEqual(element.text,'Cross-Site Scripting (XSS)- How it works')
		"""

    

    def tearDown(self):
        self.driver.quit()
        

if __name__ == "__main__":
    unittest.main()
