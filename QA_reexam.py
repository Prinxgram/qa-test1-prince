from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
from selenium.webdriver.support import expected_conditions as EC




class QAExam(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Chrome("C:\\Users\\User\\FILES\\2ndyear\\SOFTWAREQUALITYENG\\chromedriver.exe") 
        self.driver.implicitly_wait(50) # use for loading the page with the given time of 30 sec.
        self.base_url = "https://danibudi.github.io/Cross-Site%20Scripting%20(safe%20XSS).html"
        
    
    def test_ss(self):
        driver = self.driver
        driver.get(self.base_url)
        time.sleep (10)

        alert = driver.switch_to_alert()
        alert.accept()
        #print('myalert', alert.text)

        driver.save_screenshot("First_Screenshot.png")

        
        elm = driver.find_element_by_link_text('Click here to see solution of problem in Django')
        driver.implicitly_wait(10) # use for loading the page with the given time of 30 sec.
        elm.click()
        time.sleep (10)



       
        #try:
            #link = WebDriverWait(driver, 5).until(
                #EC.presence_of_element_located((By.LINK_TEXT, 'djangoproject'))
            #)
        #finally:
            #self.assertTrue(link.is_displayed)
            #if(os.path.exists('./shots') == False):
                #os.mkdir('./shots')
                assert 'django' in driver.title

        
        
        

       
    

    def tearDown(self):
        self.driver.quit()
        

if __name__ == "__main__":
    unittest.main()